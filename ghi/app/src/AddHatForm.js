import React, { useState, useEffect } from 'react';

const AddHatForm = () => {
    const [formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        picture_url: '',
        location: '',
    });
    const [locations, setLocations] = useState([]);

    const getLocations = async () => {
        const url = 'http://localhost:8100/api/locations/';
        try {
            const res = await fetch(url);
            if (res.ok) {
                const { locations } = await res.json();
                setLocations(locations);
            }
        } catch (e) {
            console.log("An error occurred", e);
        }
    }

    useEffect(() => {
        getLocations();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        try {
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    fabric: '',
                    style: '',
                    color: '',
                    picture_url: '',
                    location: '',
                });
            }
        } catch (e) {
            console.log("Error", e);
        }
    }

    const handleFormChange = ({ target }) => {
        const { value, name } = target;
        setFormData({
            ...formData,
            [name]: value
        });
    }

    const { fabric, style, color, picture_url:pictureUrl, location } = formData;

    return (
        <div>
            <h1>Add Hat to Location</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="fabric" className="form-label">Fabric:</label>
                    <input value={fabric} onChange={handleFormChange} type="text" className="form-control" name="fabric"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="style" className="form-label">Style:</label>
                    <input value={style} onChange={handleFormChange} type="text" className="form-control" name="style"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="color" className="form-label">Color:</label>
                    <input value={color} onChange={handleFormChange} type="text" className="form-control" name="color"></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="picture_url" className="form-label">Picture URL:</label>
                    <input value={pictureUrl} onChange={handleFormChange} type="text" className="form-control" name="picture_url"></input>
                </div>
                <div>
                    <label htmlFor="location" className="form-label">Location</label>
                    <select value={location} onChange={handleFormChange} className="form-control" id="location">
                        <option value="">Select a location</option>
                        {locations.map(({ id, closet_name, section_number }) => (
                            <option value={id} key={id + closet_name}>{closet_name} - location {section_number}</option>
                        ))}
                    </select>
                </div>
                <button className="btn btn-lg btn-primary w-100" type="submit">Create Hat</button>
            </form>
        </div>
    )
}

export default AddHatForm;
