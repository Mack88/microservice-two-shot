import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import AddHatForm from './AddHatForm'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats/new/" element={<AddHatForm />} />
          <Route path="/hats" index element={<HatList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
