import React, { useState, useEffect } from 'react';
import HatListItem from './HatListItem';

const HatList = function(props) {
    const [hats, setHats] = useState([]);

    const getHats = async function() {
        const url = 'http://localhost:8090/api/hats/';
        try {
            const response = await fetch(url);
            const hatsData = await response.json();
            setHats(hatsData);
        } catch (error) {
            console.error('An error occurred while fetching hats:', error);
        }
    }

    const deleteHat = async function(id) {
        const url = `http://localhost:8090/api/hats/${id}/`

        try {
            const res = await fetch(url, {method: 'DELETE'})
            if (res.ok) {
                const newHats = hats.filter(hat => hat.id !== id);
                setHats(newHats);
            }
        } catch(e) {
            console.error('An error occurred deleting the hat:', e);
        }
    }

    useEffect(() => {
        getHats();
    }, [])

    return(
        <>
            <h1>Hats</h1>
            <div className="row">
                {hats.map(hat => <HatListItem key={hat.id} hat={hat} deleteHat={deleteHat} />)}
            </div>
        </>
    )
}

export default HatList;
