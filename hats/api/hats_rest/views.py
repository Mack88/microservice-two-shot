from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        # "closet_name",
        # "shelf_number",
        # "section_number",
        "import_href",
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "style", "fabric","color","picture_url", "location"]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(hats, encoder=HatDetailEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            location_href = f'/api/locations/{content["location"]}/'
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            message = f'No matching LocationVO found {content["location"]}'
            return JsonResponse({"message": message}, status=404)
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, HatDetailEncoder, safe=False, status=201)

@require_http_methods(["DELETE"])
def detail_hats(request, pk=None):
    if request.method == "DELETE":
        deleted, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted" : deleted > 0})
