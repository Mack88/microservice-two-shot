from django.db import models
from django.urls import reverse



# Create your models here.

#
class BinVO(models.Model):
    import_href =  models.CharField(max_length=200, unique=True)


class Shoe(models.Model):
    model_name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    bin_location = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE
        )

    def get_api_url(self):
        return reverse("detail_shoes", kwargs={"pk": self.pk})
