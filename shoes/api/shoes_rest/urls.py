from django.urls import path
from .views import api_detail_shoe, api_list_shoes


urlpatterns = [
    path("shoes/", api_list_shoes, name="list_shoes"),
    path("shoes/<int:pk>/", api_detail_shoe, name="detail_shoe")
]
