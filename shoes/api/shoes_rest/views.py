from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json
# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "import_href",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "bin",
        "picture_url",
    ]
    encoders={
        "bin": BinVOEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:#POST


        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{request.GET["bin"]}/'
            bin_vo = BinVO.objects.get(import_href=bin_href)
            content['bin'] = bin_vo

        except BinVO.DoesNotExist:
            message = f'Bin {bin_href} does not exist'
            return JsonResponse(
                {"message": message},
                status=404
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
            status=201,
        )

@require_http_methods(["DELETE"])
def api_detail_shoe(request, pk = None):
    if request.method == "DELETE":
       deleted, _ = Shoe.objects.filter(id=pk).delete()
       return JsonResponse({"deleted": deleted > 0})
